Main

        node ~/distributed-systems/main/basic.js

Load tester

        cd ~/distributed-systems/loadtest && java Reset
        cd ~/distributed-systems
        jmeter -n -t ~/distributed-systems/loadtest/test.jmx -l /path/to/results.jtl

External Service

        source loadtest/fail.sh