import os
import pandas as pd
import matplotlib.pyplot as plt

def read_csv_files(folder_path):
    latency_data = pd.DataFrame()
    throughput_data = pd.DataFrame()

    latency_file_path = os.path.join(folder_path, 'latency.csv')
    if os.path.exists(latency_file_path):
        latency_data = pd.read_csv(latency_file_path)

    throughput_file_path = os.path.join(folder_path, 'throughput.csv')
    if os.path.exists(throughput_file_path):
        throughput_data = pd.read_csv(throughput_file_path)

    return latency_data, throughput_data

def main():
    folders = ['set1', 'set2', 'set3', 'set4', 'set5', 'set6', 'set7']
    for folder in folders:
        folder_path = os.path.join(os.getcwd(), folder)
        if os.path.exists(folder_path):
            latency_data, throughput_data = read_csv_files(folder_path)
            
            if not latency_data.empty:
                plt.figure(figsize=(10, 6))
                plt.plot(latency_data['timestamp'], latency_data['latency'], label='Latency')
                plt.title(f"Latency and Throughput Data: {folder}")
                plt.xlabel('Timestamp')
                plt.ylabel('Latency')
                plt.grid(True)
                plt.legend()
                plt.show()
            else:
                print(f"No latency data found in {folder}")

            if not throughput_data.empty:
                plt.figure(figsize=(10, 6))
                plt.plot(throughput_data['timestamp'], throughput_data['throughput'], label='Throughput', color='orange')
                plt.title(f"Throughput Data: {folder}")
                plt.xlabel('Timestamp')
                plt.ylabel('Throughput')
                plt.grid(True)
                plt.legend()
                plt.show()
            else:
                print(f"No throughput data found in {folder}")
        else:
            print(f"Folder '{folder}' does not exist.")

if __name__ == "__main__":
    main()
