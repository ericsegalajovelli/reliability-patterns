const express = require('express');
const axios = require('axios');
const {createClient} = require('redis');
const app = express();
const port = 8080;

(async function main () {

const instance = axios.create({
    baseURL: "http://10.10.1.1:3002",
});

const redisClient = await createClient({url: process.env.REDIS_URL})
  .on('error', err => console.log('Redis Client Error', err))
  .connect();

app.get('/code/:id', async (req, res) => {
  try {
    const data = await redisClient.get(req.params.id)
    if (data) {
      res.json(JSON.parse(data));
    } else {
      try {
        const response = await instance.get("code/"+req.params.id);
        if (response.data) {
          redisClient.set(req.params.id, JSON.stringify(response.data));
        }
        res.json(response.data);
      } catch (error) {
        console.log(error)
        res.status(503).json({ error: 'External service unavailable' });
      }
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({ error: 'Internal server error' });
  }
});

app.listen(port, () => {
  console.log(`Cache API listening at http://localhost:${port}`);
});
})();
