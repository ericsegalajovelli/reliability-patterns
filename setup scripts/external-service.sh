# Install mongo, Node and pm2
echo "deb http://security.ubuntu.com/ubuntu focal-security main" | sudo tee /etc/apt/sources.list.d/focal-security.list;
sudo apt-get update;
sudo apt-get install -y libssl1.1;
curl -fsSL https://www.mongodb.org/static/pgp/server-4.4.asc | \
   sudo gpg -o /usr/share/keyrings/mongodb-server-4.4.gpg \
   --dearmor;
echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-4.4.gpg ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list;
sudo apt-get update;
sudo apt-get install -y mongodb-org=4.4.29 mongodb-org-server=4.4.29 mongodb-org-shell=4.4.29 mongodb-org-mongos=4.4.29 mongodb-org-tools=4.4.29;
curl -sL https://deb.nodesource.com/setup_18.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh;
sudo apt-get install nodejs -y;
bash;
npm i -g pm2;

# setup MongoDb
sudo vi /etc/mongod.conf;
# Edit to allow authentication
security:
  authorization: enabled

#in the shell:
mongo
use admin

db.createUser({
  user: "user",
  pwd: "uyh98gh897gt789gOO",
  roles: [ { role: "root", db: "admin" } ]
})

# get ip
# ifconfig;
