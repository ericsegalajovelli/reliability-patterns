#!/bin/bash

# Install jmeter
sudo apt update
sudo apt upgrade
sudo apt install openjdk-11-jdk -y
java -version
cd ~
wget https://downloads.apache.org/jmeter/binaries/apache-jmeter-5.6.3.tgz
tar -xvzf apache-jmeter-5.6.3.tgz
mv apache-jmeter-5.6.3 apache-jmeter
echo 'export JMETER_HOME=~/apache-jmeter/' >> ~/.bashrc
echo 'export PATH=$JMETER_HOME/bin:$PATH' >> ~/.bashrc

#run jmeter
jmeter -n -t ~/distributed-systems/loadtest/test.jmx -l ~/distributed-systems/loadtest/results.jtl

# scp
scp ericjov@pc541.emulab.net:~/distributed-systems/loadtest/results.jtl C:\Users\erics\OneDrive\Documentos