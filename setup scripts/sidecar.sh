# install docker
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu jammy stable"
apt-cache policy docker-ce
sudo apt install docker-ce
sudo systemctl status docker

# cache
sudo docker build -t meu-servidor-cache .
sudo docker run -p 8080:8080 meu-servidor-cache

sudo docker compose up --build