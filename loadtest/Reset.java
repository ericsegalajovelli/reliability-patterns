import java.util.concurrent.atomic.AtomicLong;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.io.BufferedReader;
import java.net.URL;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Date;
import java.util.Random;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.Paths;
import java.util.Properties;

class Populate {

	private static final String USER_AGENT = "Mozilla/5.0";
    private static final String HEX_CHARS = "0123456789abcdef";

    private static String generateObjectId() {
        long timestamp = System.currentTimeMillis() / 1000;
        StringBuilder objectId = new StringBuilder(24);
        appendHexTimestamp(objectId, timestamp);
        Random random = new Random();
        for (int i = 0; i < 16; i++) {
            objectId.append(HEX_CHARS.charAt(random.nextInt(16)));
        }
        return objectId.toString();
    }

    private static String generatePayload(int size) {
        StringBuilder objectId = new StringBuilder(size);
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            objectId.append(HEX_CHARS.charAt(random.nextInt(16)));
        }
        return String.format("{ \"code\": \"%s\" }", objectId.toString());
    }

    private static void appendHexTimestamp(StringBuilder sb, long timestamp) {
        for (int i = 7; i >= 0; i--) {
            sb.append(HEX_CHARS.charAt((int) (timestamp >> (i * 4)) & 0xf));
        }
    }

	private int numberOfRegisters;
	private int payloadTextSize;
    
    public Populate(int numberOfRegisters, int payloadTextSize, String path) throws IOException, InterruptedException {
        this.numberOfRegisters = numberOfRegisters;
        this.payloadTextSize = payloadTextSize;
        String PathIdsFile = "./ids.csv";
                File file = new File(PathIdsFile);
            if (file.exists()) {
            boolean deleted = file.delete();
            if (deleted) {
                System.out.println("File deleted successfully.");
            } else {
                System.out.println("Failed to delete the file.");
            }
        } else {
            System.out.println("File does not exist.");
        }
        FileWriter fileWriterIds = new FileWriter(PathIdsFile);
        BufferedWriter bufferedWriterIds = new BufferedWriter(fileWriterIds);

        for (long i = 0; i < numberOfRegisters; i++) {
            try {
                String objId = generateObjectId();
                URL obj = new URL(path + objId);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("PUT");
                con.setRequestProperty("User-Agent", USER_AGENT);
                con.setRequestProperty("Content-Type", "application/json");
                con.setDoOutput(true);
                OutputStream os = con.getOutputStream();
                os.write(generatePayload(payloadTextSize).getBytes());
                os.flush();
                os.close();
                int responseCode = con.getResponseCode();
                System.out.println("POST Response Code :: " + responseCode);
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                    bufferedWriterIds.write(String.format("%s\n", objId));
                    System.out.println(response.toString());
                } else {
                    System.out.println("POST request did not work.");
                    System.out.println("Error: " + responseCode);
                }
                Thread.sleep(10);
                con.disconnect();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
        bufferedWriterIds.close();
    }
}
class Wipe {

	private static final String USER_AGENT = "Mozilla/5.0";
    
    public void HttpRequest(String httpVerb, String path) {
    try {
      URL url = new URL(path);
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setRequestMethod(httpVerb);
      con.setRequestProperty("User-Agent", USER_AGENT);
      int responseCode = con.getResponseCode();
      if (responseCode == HttpURLConnection.HTTP_OK) {
        System.out.println("Wiped!");
      } else {
        System.out.println("Wipe failed: status " + responseCode);
      }
        con.disconnect();
    } catch (IOException e) {
      e.printStackTrace();
    }
    }

     Wipe(String path) {
            HttpRequest("DELETE", path);
    System.out.println("Wiping");
    }
}

public class Reset {
    public static void main(String[] args) throws IOException {
        try {
          for (int i = 1; i <= 1; i++) {
            String fileName = "set" + i + ".config";
            String filePath = Paths.get("configs", fileName ).toString();
            File file = new File(filePath);
            if (file.exists()) {
                    BufferedReader reader = new BufferedReader(new FileReader(file));
                    StringBuilder content = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        content.append(line).append("\n");
                    }
                    Properties prop = new Properties();
                    try (FileInputStream fis = new FileInputStream(filePath)) {
                      prop.load(fis);
                    } catch (FileNotFoundException ex) {
                      ex.printStackTrace();
                    } catch (IOException ex) {
                      ex.printStackTrace();
                  }
                    reader.close();
                    Wipe wipe = new Wipe(prop.getProperty("url"));
                    Populate populate = new Populate(5000,20, prop.getProperty("url"));
            } else {
                System.out.println("File " + fileName + " not found.");
            }
                System.out.println("finished");
        }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}