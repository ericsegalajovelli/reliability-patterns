const express = require("express");
const CircuitBreaker = require('opossum');
const axios = require('axios').default;
const app = express();
app.use(express.json());
if (!process?.argv?.[2]) {
  throw "please add the domain in the command args"
}
let url = `http://${process?.argv?.[2]}:3002`


const options = {
  timeout: 3000,
  errorThresholdPercentage: 50,
  resetTimeout: 3000
};
const instance = axios.create({
  baseURL: url,
});

const getById = async (req, res) => {
  console.log("GET to external API", req.params.id);
  const response = await instance.get("code/"+req.params.id);
  return response.data;
}

const upsert = async (req, res) => {
  console.log("PUT to external API", req.params.id, req.body);
  const code = await instance.put("code/"+req.params.id,req.body)
  return code.data
}
  
const getByIdBreaker = new CircuitBreaker(getById, options);
const upsertBreaker = new CircuitBreaker(upsert, options);

getByIdBreaker.on('open', () => console.log('Circuit breaker for getById opened'));
getByIdBreaker.on('halfOpen', () => console.log('Circuit breaker for getById half-opened'));
getByIdBreaker.on('close', () => console.log('Circuit breaker for getById closed'));
upsertBreaker.on('open', () => console.log('Circuit breaker for upsert opened'));
upsertBreaker.on('halfOpen', () => console.log('Circuit breaker for upsert half-opened'));
upsertBreaker.on('close', () => console.log('Circuit breaker for upsert closed'));

app.get("/code/:id", (req,res) => {
  console.log('api got GET request')
  getByIdBreaker.fire(req,res)
  .then(r => {
    console.log('GET returning response')
    res.json(r)
  })
  .catch(() => {
    console.log('GET returning 503')
    res.status(503).send()
  })
})

app.put("/code/:id", (req,res) => {
  console.log("api got PUT request")
  upsertBreaker.fire(req,res)
  .then(r => {
    console.log('PUT returning response')
    res.json(r)
  })
  .catch(() => {
    console.log('PUT returning 503')
    res.status(503).send()
  })
})

const PORT = 3003;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
