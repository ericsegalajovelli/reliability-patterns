const express = require("express");
const retry = require("retry");
const axios = require('axios').default;
const app = express();
app.use(express.json());
if (!process?.argv?.[2]) {
  throw "please add the domain in the command args"
}
let url = `http://${process?.argv?.[2]}:3002`

const instance = axios.create({
  baseURL: url,
});

app.get("/code/:id", async (req, res) => {
  console.log("GET", req.params.id);
  var operation = retry.operation();
  operation.attempt(async function() {
    try {
      const response = await instance.get("code/"+req.params.id);
      res.json(response.data)
    } catch (error) {
      console.log(error)
      if (operation.retry(error)) {
        return;
      }
      res.status(500).send(error);
    }
  });
});

app.put("/code/:id", async (req, res) => {
  var operation = retry.operation();
  operation.attempt(async function() {
    try {
      console.log("PUT", req.params.id, req.body);
      const code = await instance.put("code/"+req.params.id,req.body)
      res.json(code.data)
    } catch (error) {
      console.log(error)
      if (operation.retry(error)) {
        return;
      }
      res.status(500).send(error);
    }
  });
});

const PORT = 3003;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
