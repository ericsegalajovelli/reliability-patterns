const express = require("express");
const axios = require('axios').default;
const app = express();
app.use(express.json());

app.get("/code/:id", async (req, res) => {
  try {
    const url = `http://localhost:8080`
    console.log("GET", req.params.id);
    const response = await axios.get(url + "/code/"+req.params.id);
    res.json(response.data);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

app.put("/code/:id", async (req, res) => {
  try {
    const url = `http://10.10.1.1:3002`
    console.log("PUT", req.params.id, req.body);
    const code = await axios.put(url+"/code/"+req.params.id,req.body)
    res.json(code.data);
  } catch (error) {
    console.log(error);
    if (error.response.status === 503) {
      res.status(503).send(error);
    }
    res.status(500).send(error);
  }
});

const PORT = 3003;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
