const express = require("express");
const axios = require('axios').default;
const app = express();
app.use(express.json());
if (!process?.argv?.[2]) {
  throw "please add the domain in the command args"
}
let url = `http://${process?.argv?.[2]}:3002`


const instance = axios.create({
  baseURL: url,
});

app.get("/code/:id", async (req, res) => {
  try {
    console.log("GET", req.params.id);
    const response = await instance.get("code/"+req.params.id);
    res.json(response.data);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

app.put("/code/:id", async (req, res) => {
  try {
    console.log("PUT", req.params.id, req.body);
    let result = 0;
    for (let i = 0; i < 1e8; i++) {
      result += i;
    }
    res.json({result});
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

const PORT = 3003;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
