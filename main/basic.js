const express = require("express");
const axios = require('axios').default;
const app = express();
app.use(express.json());
if (!process?.argv?.[2]) {
  throw "please add the domain in the command args"
}
let url = `http://${process?.argv?.[2]}:3002`


app.get("/code/:id", async (req, res) => {
  try {
    console.log("GET", req.params.id);
    const response = await axios.get(url + "/code/"+req.params.id);
    res.json(response.data);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

app.put("/code/:id", async (req, res) => {
  try {
    console.log("PUT", req.params.id, req.body);
    const code = await axios.put(url+"/code/"+req.params.id,req.body)
    res.json(code.data);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

const PORT = 3003;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
