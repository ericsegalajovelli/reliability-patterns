const express = require("express");
const mongoose = require("mongoose");
const app = express();
app.use(express.json());

url = "mongodb://user:uyh98gh897gt789gOO@localhost:27017/admin"

const dbOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  serverSelectionTimeoutMS: 5000
}

mongoose.connect(url, dbOptions);

const db = mongoose.connection;

function reconnect() {
  mongoose.connect(url, dbOptions)
    .catch(() => console.error('Failed to reconnect to MongoDB.'));
}

db.on("error", (error) => {
  console.error("Connection error:", error);
  setTimeout(reconnect, 5000);
});

db.on('disconnected', reconnect);

db.once("open", function () {
  console.log("Connected successfully to MongoDB");
});

const Schema = mongoose.Schema;
const CodeSchema = new Schema({
  code: String,
});

const Code = mongoose.model("Code", CodeSchema);

const ObjectId = require("mongoose").Types.ObjectId;

function isValidObjectId(id) {
  if (ObjectId.isValid(id)) {
    if (String(new ObjectId(id)) === id) return true;
    return false;
  }
  return false;
}

app.get("/code/:id", async (req, res) => {
  try {
    console.log("GET", req.params.id);
    const code = await Code.findById(req.params.id);
    res.json(code);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

app.put("/code/:id", async (req, res) => {
  try {
    console.log("PUT", req.params.id, req.body);
    const validId = isValidObjectId(req.params.id)
      ? req.params.id
      : new ObjectId();
    const code = await Code.findByIdAndUpdate(validId, req.body, {
      new: true,
      upsert: true,
    });
    res.json(code);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

app.delete("/code", async (req, res) => {
  try {
    console.log("DELETE /wipe", req.params.id, req.body);
    await Code.deleteMany();
    res.json({ deleted: true });
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

const PORT = 3002;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
